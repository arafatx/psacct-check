#!/bin/sh
#psacct-check.sh - Process Accounting Check
#By Arafat Ali
#Version 1.0

#Global Variables
MYFILENAME="psacct-report.txt"
MYHOSTNAME=`/bin/hostname`
MYEMAIL="webmaster@sofibox.com"
CURRENT_IP_LOGIN_COUNT="0"
WARNING_STATUS="ERROR-STATUS" #FOR COUNTING HOST LOGINS

#Config file for 2 parameters setting
function set_config(){
    sudo sed -i "s/^\($1\s*=\s*\).*\$/\1$2/" $CONFIG
}

CONFIG="/usr/local/bin/maxicron/psacct-config.conf"

# Check config if exist
if [ ! -e "${CONFIG}" ] ; then
    # Set default variable value
    sudo touch $CONFIG
    echo "HOST_IP_LOGIN_COUNT=\"0\"" | sudo tee --append $CONFIG
fi

#Load the config file
source /usr/local/bin/maxicron/psacct-config.conf

/bin/echo "======================================"
/bin/echo "[psacct] Process Accounting is checking system..."
#/bin/echo "HOST IP LOGIN COUNT IS: $HOST_IP_LOGIN_COUNT"

#let HOST_IP_LOGIN_COUNT=$HOST_IP_LOGIN_COUNT+1

#set_config HOST_IP_LOGIN_COUNT $HOST_IP_LOGIN_COUNT

/bin/echo "Process Accounting checked on `date`" > /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME

/bin/echo "=======================================">> /tmp/$MYFILENAME
/bin/echo "User login and logout time (in hours):" >> /tmp/$MYFILENAME
/bin/echo "" >> /tmp/$MYFILENAME
/bin/ac -p >> /tmp/$MYFILENAME
/bin/echo "**************************************" >> /tmp/$MYFILENAME

/bin/echo "" >> /tmp/$MYFILENAME

/bin/echo "=======================================">> /tmp/$MYFILENAME
/bin/echo "User last commands:" >> /tmp/$MYFILENAME
/bin/echo "1) ### user1 ###" >> /tmp/$MYFILENAME
#If you don't specify head, you are going to get a very long result. The head command by default sort it to 10 last result
/bin/lastcomm user1 | /bin/awk '{print $1}' | /bin/sort | /bin/uniq -c  | /bin/sort -nr | /bin/head >> /tmp/$MYFILENAME
/bin/echo "2) ### admin ###">> /tmp/$MYFILENAME
/bin/lastcomm user2 | /bin/awk '{print $1}' | /bin/sort | /bin/uniq -c  | /bin/sort -nr | /bin/head >> /tmp/$MYFILENAME
/bin/echo "user2 ###">> /tmp/$MYFILENAME
/bin/lastcomm user2 | /bin/awk '{print $1}' | /bin/sort | /bin/uniq -c  | /bin/sort -nr | /bin/head >> /tmp/$MYFILENAME
/bin/echo "4) ### user3 ###">> /tmp/$MYFILENAME
/bin/lastcomm user3 | /bin/awk '{print $1}' | /bin/sort | /bin/uniq -c  | /bin/sort -nr | /bin/head >> /tmp/$MYFILENAME

# Put others here. Change your username user1, user2, user3... and so on
/bin/echo "**************************************" >> /tmp/$MYFILENAME

/bin/echo "" >> /tmp/$MYFILENAME

/bin/echo "======================================">> /tmp/$MYFILENAME
/bin/echo "User Logins: " >> /tmp/$MYFILENAME
/bin/last | /bin/awk '{print $1}' | /bin/sort | /bin/uniq -c  | /bin/sort -nr | /bin/head >> /tmp/$MYFILENAME
/bin/echo "-- -- -- -- -- -- -- -- -- -- -- -- --">> /tmp/$MYFILENAME
/bin/echo "Host Logins: " >> /tmp/$MYFILENAME
/bin/last | /bin/awk '{print $3}' | /bin/sort | /bin/uniq -c  | /bin/sort -nr | /bin/head >> /tmp/$MYFILENAME
#/bin/last | /bin/awk '{print $3}' | /bin/sort | /bin/uniq -c  | wc -l
/bin/echo "**************************************">> /tmp/$MYFILENAME

/bin/echo "" >> /tmp/$MYFILENAME

CURRENT_IP_LOGIN_COUNT=`/bin/last | /bin/awk '{print $3}' | /bin/sort | /bin/uniq -c  | wc -l`

COUNT_IP="$(($CURRENT_IP_LOGIN_COUNT-$HOST_IP_LOGIN_COUNT))"
if [[ "$CURRENT_IP_LOGIN_COUNT" -eq "$HOST_IP_LOGIN_COUNT" ]] ; then
   WARNING_STATUS="OK"
   /bin/echo "[$WARNING_STATUS]: There is no new host login IP detected" >> /tmp/$MYFILENAME
else
   WARNING_STATUS="WARNING"
   if [[ "$COUNT_IP" -eq 1 ]]; then
   /bin/echo "[$WARNING_STATUS]: There is $COUNT_IP login IP detected @ $MYHOSTNAME. Please audit the IP!" >> /tmp/$MYFILENAME
   elif [[ "$COUNT_IP" -ge 2 ]]; then
   /bin/echo "[$WARNING_STATUS]: There are $COUNT_IP login IPs detected @ $MYHOSTNAME. Please audit the IPs!" >> /tmp/$MYFILENAME
   else
   WARNING_STATUS="ERROR"
   /bin/echo "[$WARNING_STATUS]: Error getting the status of the current IP. Please audit the IPs manually!" >> /tmp/$MYFILENAME
   fi
fi

#SET THE CONFIG MUST BE THE SAME
set_config HOST_IP_LOGIN_COUNT $CURRENT_IP_LOGIN_COUNT

/bin/echo "" >> /tmp/$MYFILENAME
/bin/echo "*************** DONE *****************">> /tmp/$MYFILENAME
/bin/mail -s "[psacct][$WARNING_STATUS|$COUNT_IP] Process Accounting Scan Report @ $MYHOSTNAME" $MYEMAIL < /tmp/$MYFILENAME
/bin/rm -rf /tmp/$MYFILENAME
/bin/echo "[psacct] Scan status: $WARNING_STATUS"
/bin/echo "[psacct] Done checking system. Email is set to be sent to $MYEMAIL"
/bin/echo "================================="
